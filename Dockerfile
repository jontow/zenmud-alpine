################################################################################
#
# docker build -t jontow/jontow:zenmud-alpine .
# docker run -it -u 10002000:0 jontow/jontow:zenmud-alpine
#
################################################################################

FROM docker.io/alpine:latest
MAINTAINER Jonathan Towne <jontow@mototowne.com>

# These labels are used by OpenShift in order to display information inside the project
LABEL io.k8s.description="Just an old MUD codebase in a modern world" \
 io.k8s.display-name="ZenMUD" \
 io.openshift.expose-services="8080:tcp"

# Install and configure nginx
RUN apk add nginx && chgrp -R 0 /var/log/nginx && chmod -R g=u /var/log/nginx && chgrp -R 0 /var/lib/nginx && chmod -R g=u /var/lib/nginx && chgrp -R 0 /var/tmp/nginx && chmod -R g=u /var/tmp/nginx && chgrp 0 /run && chmod g=u /run && rm /etc/nginx/conf.d/default.conf
COPY nginx.conf /etc/nginx/nginx.conf
COPY nginx_ws.conf /etc/nginx/conf.d/nginx_ws.conf

# Install and configure websocketd
RUN echo 1 && wget -O /opt/websocketd-0.3.1-linux_amd64.zip https://github.com/joewalnes/websocketd/releases/download/v0.3.1/websocketd-0.3.1-linux_amd64.zip && mkdir /opt/websocketd && unzip -d /opt/websocketd /opt/websocketd-0.3.1-linux_amd64.zip
COPY svc_conn.sh /opt/websocketd/svc_conn.sh

# Install and configure zenmud
RUN mkdir -p /opt/zenmud
COPY mudrun.sh /opt/zenmud/mudrun.sh
RUN echo 1 && wget -O /opt/zenmud-alpine-latest-x86_64-bin.tgz https://slite.zenbsd.net/~jontow/zenmud-alpine-latest-x86_64-bin.tgz && tar zxvf /opt/zenmud-alpine-latest-x86_64-bin.tgz -C /opt && chgrp -R 0 /opt && chmod -R g=u /opt

EXPOSE 8080

ENTRYPOINT ["/bin/sh", "/opt/zenmud/mudrun.sh"]
